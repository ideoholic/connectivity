/**
 * 
 */
package org.ideoholic.flights.executor;

/**
 * The main program that can be used to run the main program
 * 
 * @author ideoholic
 *
 */
public class MainRunner {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // Main execution starts here
    }

}
