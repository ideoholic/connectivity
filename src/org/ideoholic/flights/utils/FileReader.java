package org.ideoholic.flights.utils;

/**
 * This class is used to read file and get the data in required format.
 * 
 * @author ideoholic
 *
 */
public class FileReader {

    private String fileName;

    public FileReader(String fileName) {
        this.fileName = fileName;
    }

}
