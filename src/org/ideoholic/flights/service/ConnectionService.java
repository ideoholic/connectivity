/**
 * 
 */
package org.ideoholic.flights.service;

import org.ideoholic.flights.data.Connector;

/**
 * This is the class that checks if the given sites are connected
 * Additional methods as and when needed
 * @author ideoholic
 *
 */
public class ConnectionService {

    /**
     * This is the method that checks for the connection to exist between two
     * connectors.<br/>
     * One can change method signature if needed
     * 
     * @param source
     * @param destination
     * @return
     */
    public boolean checkIfNodesAreConnected(Connector source, Connector destination) {
        return false;
    }

}
