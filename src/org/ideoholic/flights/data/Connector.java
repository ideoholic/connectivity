/**
 * 
 */
package org.ideoholic.flights.data;

import java.util.List;

/**
 * Container class that can be used to store data to be manipulated. This data
 * is loaded and then logic written to use this class.
 * 
 * @author ideoholic
 *
 */
public class Connector {

    private String name;
    private List<String> connectedTo;

    /**
     * 
     */
    public Connector(String[] data) {}

    public String getName() {
        return name;
    }

    public List<String> getConnections() {
        return connectedTo;
    }

}
