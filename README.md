# Connectivity

There are airports that are connected to each other. Each airport is marked by a name. The adjoining text file contains the connecting ports. The first item is the source and the rest of the elements are the ones that it is connected to.

The goal is to find if two airports are connected to each other when two ports are given as input.
